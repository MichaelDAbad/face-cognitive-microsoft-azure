<?php
$file = $_FILES['file'];
$response = array();
if (copy($file['tmp_name'], 'img'.$file['name'])) {
	$response['success'] = true;
	$response['img_name'] = $file['name'];
} else {
	$response = array(
		'success' => false,
		'message' => 'Error al subir archivo'
	);
}

echo json_encode($response);
?>